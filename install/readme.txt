Na SD disku naleznete:

- readme.txt 	- stručný popis obsahu CD disku,
- exe			- adresář se spustitelnou formou implementace
- Opendata 		- adresář projektu se zdrojovými kódy
- thesis		- zdrojová forma práce ve formátu LatexvalidateIco
- text			- text práce

Předpoklady pro spuštění aplikace:

- nainstalovaný databázový systém PostgreSQL. Verze: "PostgreSQL 9.6.3, compiled by Visual C++ build 1800, 64-bit"
- nainstalována Java verze 1.8.0_131, "java version "1.8.0_131""

Nastavení databáze:

- spojení s produkční databází je definováno v souboru Opendata\src\main\resources\application.properties,
- pro produkční prostředí defaultní název databáze je "opendata_new", defaultní username "postgres", defaultní heslo je "80506074004Bn",
- spojení s databází pro testy je definováno v souboru OpendataBP\src\test\resources\application-test.properties,
- pro prostředí pro testy defaultní název databáze je "opendata_test", defaultní username "postgres", defaultní heslo je "80506074004Bn".

Vytvoření produkční databáze lze provést pomocí následujícího instalačního scriptu (dump produkční databáze):

- Opendata\sql\full_script.sql - vytvoření schéma (opendata_new) a tabulek, kde jako majitel se nastavuje uživatel "postgres", načtení produkčních dat do tabulek. provedení scriptu trvá cca. 20 minut. Spuštění scriptu lze provést příkazem "\i cesta_ke_složce_opendata/Opendata/sql/full_script.sql".

Vytvoření databáze pro testy lze provést pomocí následujícího instalačního scriptu (dump databáze pro testy):

- Opendata\sql\test_full_script.sql - vytvoření schéma (opendata_test) a tabulek, kde jako majitel se nastavuje uživatel "postgres", načtení dat pro testy do tabulek. provedení scriptu trvá cca. 1 minutu. Spuštění scriptu lze provést příkazem "\i cesta_ke_složce_opendata/Opendata/sql/test_full_script.sql".

Spuštění aplikace:

- spustitelný JAR soubor pod názvem "opendata-0.0.1-SNAPSHOT.jar" se nachazí ve složce exe. Po vytvoření databáze aplikaci je možné spustit pomocí příkazu: ""cesta_ke_jave" -Dfile.encoding=windows-1250 -jar exe\opendata-0.0.1-SNAPSHOT.jar"