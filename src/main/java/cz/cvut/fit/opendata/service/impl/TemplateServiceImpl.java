package cz.cvut.fit.opendata.service.impl;

import cz.cvut.fit.opendata.service.LinkResolverService;
import cz.cvut.fit.opendata.service.PaginationService;
import cz.cvut.fit.opendata.service.TemplateService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;

@Component
public class TemplateServiceImpl implements TemplateService {

    private final LinkResolverService linkResolverService;
    private final PaginationService paginationService;

    public TemplateServiceImpl(LinkResolverServiceImpl linkResolverService, PaginationServiceImpl paginationService) {
        this.linkResolverService = linkResolverService;
        this.paginationService = paginationService;
    }

    @Override
    public void fillTemplateBaseLinks(ModelAndView modelAndView, HttpServletRequest request,
                                      String templateName, Long currentPage, Long rowsCnt) {
        modelAndView.setViewName(templateName);
        fillTemplateWithPagination(modelAndView, request, currentPage, rowsCnt);
        fillTemplateHeaderLinks(modelAndView, request);
    }

    @Override
    public void fillTemplateWithPagination(ModelAndView modelAndView, HttpServletRequest request,
                                           Long currentPage, Long rowsCnt) {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(request);
        modelAndView.addObject(NEXT_PAGE, linkResolverService.getPageUri(builder, paginationService.countNextPage(currentPage, rowsCnt)));
        modelAndView.addObject(PREVIOUS_PAGE, linkResolverService.getPageUri(builder, paginationService.countPreviousPage(currentPage, rowsCnt)));
        modelAndView.addObject(FIRST_PAGE, linkResolverService.getPageUri(builder, paginationService.countFirstPage()));
        modelAndView.addObject(LAST_PAGE, linkResolverService.getPageUri(builder, paginationService.countLastPage(rowsCnt)));
        modelAndView.addObject(CURRENT_PAGE, linkResolverService.getPageUri(builder, paginationService.countCurrentPage(currentPage)));
        modelAndView.addObject(NEXT_PAGE_NUMBER, paginationService.countNextPage(currentPage, rowsCnt));
        modelAndView.addObject(PREVIOUS_PAGE_NUMBER, paginationService.countPreviousPage(currentPage, rowsCnt));
        modelAndView.addObject(FIRST_PAGE_NUMBER, paginationService.countFirstPage());
        modelAndView.addObject(LAST_PAGE_NUMBER, paginationService.countLastPage(rowsCnt));
        modelAndView.addObject(CURRENT_PAGE_NUMBER, paginationService.countCurrentPage(currentPage));

    }

    @Override
    public void fillTemplateHeaderLinks(ModelAndView modelAndView, HttpServletRequest request) {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(request);
        modelAndView.addObject(HOME_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_HOME_PAGE));
        modelAndView.addObject(DOCUMENTS_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_DOCUMENTS_PAGE));
        modelAndView.addObject(ORGANIZATION_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_ORGANIZATION_PAGE));
        modelAndView.addObject(API_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_API_PAGE));
        modelAndView.addObject(ABOUT_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_ABOUT_PAGE));
        modelAndView.addObject(BASE_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, ""));
        modelAndView.addObject(API_DOCUMENTS_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_API_DOCUMENTS));
        modelAndView.addObject(API_ORGANIZATIONS_PAGE_LINK, linkResolverService.getPageUriReplacedPath(builder, STATIC_API_ORGANIZATIONS));
    }
}
