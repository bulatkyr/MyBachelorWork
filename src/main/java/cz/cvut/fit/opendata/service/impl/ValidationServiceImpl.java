package cz.cvut.fit.opendata.service.impl;

import cz.cvut.fit.opendata.model.EntityType;
import cz.cvut.fit.opendata.model.RecordType;
import cz.cvut.fit.opendata.model.exception.ValidationException;
import cz.cvut.fit.opendata.service.ValidationService;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ValidationServiceImpl implements ValidationService {

    @Override
    public void validateNonNegativeLong(Long id) throws ValidationException {
        if (id < 0) {
            throw new ValidationException("Number exception");
        }
    }

    @Override
    public void validateRecordType(String recordType) throws ValidationException {
        for (RecordType rt : RecordType.values()) {
            if (rt.getType().equals(recordType)) {
                return;
            }
        }
        throw new ValidationException("Record Type " + recordType + " is not recognized");
    }

    @Override
    public List<String> parseRecordTypes(String recordTypes) throws ValidationException {
        List<String> result = Arrays.asList(recordTypes.split(","));
        for (String element : result) {
            validateRecordType(element);
        }
        return result;
    }

    @Override
    public List<String> parseEntityTypes(String entityTypes) throws ValidationException {
        List<String> result = Arrays.asList(entityTypes.split(","));
        for (String element : result) {
            validateEntityType(element);
        }
        return result;
    }

    @Override
    public void validateEntityType(String entityType) throws ValidationException {
        for (EntityType et : EntityType.values()) {
            if (et.getType().equals(entityType)) {
                return;
            }
        }
        throw new ValidationException("Organization Type " + entityType + " is not recognized");
    }

    @Override
    public String validateIco(String ico) throws ValidationException {
        if (ico == null) {
            throw new ValidationException("ICO is not specified");
        }
        if (!ico.matches("[0-9]+")) {
            throw new ValidationException("ICO " + ico + " has to contain only numbers");
        }
        int lengthDifference = 8 - ico.length();
        if (lengthDifference != 0) {
            StringBuilder icoBuilder = new StringBuilder(ico);
            for (int i = 0; i < lengthDifference; i++) {
                icoBuilder.insert(0, "0");
            }
            ico = icoBuilder.toString();
        }
        if (!new Integer(8).equals(ico.length())) {
            throw new ValidationException("ICO has incorrect length " + ico.length());
        }
        int checkSum = 0;
        for (int i = 0; i < 7; i++) {
            checkSum += Character.getNumericValue(ico.charAt(i)) * (8 - i);
        }
        checkSum %= 11;
        int mod11Result;
        if (checkSum == 0) {
            mod11Result = 1;
        } else if (checkSum == 1) {
            mod11Result = 0;
        } else {
            mod11Result = 11 - checkSum;
        }
        if (Character.getNumericValue(ico.charAt(7)) != mod11Result) {
            throw new ValidationException("ICO is not specified correctly");
        }
        return ico;
    }

    @Override
    public void validateStartEndRow(Long startRow, Long endRow) throws ValidationException {
        if (startRow == null || endRow == null) {
            throw new ValidationException("Start/end row values are not set correct.");
        }
        if (startRow > endRow) {
            throw new ValidationException("Start row value must be less or equal to end row value.");
        }
    }
}
