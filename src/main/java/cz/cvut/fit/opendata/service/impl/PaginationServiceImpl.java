package cz.cvut.fit.opendata.service.impl;

import cz.cvut.fit.opendata.model.exception.ValidationException;
import cz.cvut.fit.opendata.service.PaginationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PaginationServiceImpl implements PaginationService {

    private static final Logger log = LoggerFactory.getLogger(PaginationServiceImpl.class);

    public void validateCurrentPage(Long currentPage, Long rowsCnt) throws ValidationException {
        if (currentPage == null || currentPage < 0) {
            throw new ValidationException("Current page " + currentPage + " is null or negative.");
        }
        if (rowsCnt == null || rowsCnt < 0) {
            throw new ValidationException("Rows count " + rowsCnt + " is null or negative.");
        }
        Long lastPage = countLastPage(rowsCnt);
        if (currentPage > lastPage) {
            throw new ValidationException("Current page " + currentPage + " cannot be higher than last page value.");
        }
    }

    public Long countNextPage(Long currentPage, Long rowsCnt) {
        try {
            validateCurrentPage(currentPage, rowsCnt);
        } catch (ValidationException e) {
            log.debug("Counting next page failed for currentPage = {} and rowsCnt = {}", currentPage, rowsCnt);
            return currentPage;
        }
        Long lastPage = countLastPage(rowsCnt);
        if (currentPage.equals(lastPage)) {
            return currentPage;
        }
        return currentPage + 1;
    }

    public Long countPreviousPage(Long currentPage, Long rowsCnt){
        try {
            validateCurrentPage(currentPage, rowsCnt);
        } catch (ValidationException e) {
            log.debug("Counting previous page failed for currentPage = {} and rowsCnt = {}", currentPage, rowsCnt);
            return currentPage;
        }
        if (currentPage <= 0) {
            return 0L;
        }
        return currentPage - 1;
    }

    public Long countStartRow(Long currentPage, Long rowsCnt) {
        try {
            validateCurrentPage(currentPage, rowsCnt);
        } catch (ValidationException e) {
            log.debug("Counting start row failed for currentPage = {} and rowsCnt = {}", currentPage, rowsCnt);
            return 1L;
        }
        if (currentPage <= 0) {
            return 1L;
        }
        return currentPage * ITEMS_PER_PAGE + 1;
    }

    public Long countEndRow(Long currentPage, Long rowsCnt) {
        try {
            validateCurrentPage(currentPage, rowsCnt);
        } catch (ValidationException e) {
            log.debug("Counting end row failed for currentPage = {} and rowsCnt = {}", currentPage, rowsCnt);
            return 1L;
        }
        if (currentPage <= 0) {
            return 10L;
        }
        return (currentPage + 1) * ITEMS_PER_PAGE;
    }

    public Long countFirstPage() {
        return 0L;
    }

    public Long countLastPage(Long rowsCnt) {
        if(rowsCnt == null || rowsCnt <= 0) {
            log.debug("Counting last page failed with rowsCnt = {}", rowsCnt);
            return 0L;
        }
        boolean fullyDivided = (rowsCnt % ITEMS_PER_PAGE) == 0;
        Long result;
        if (fullyDivided) {
            result = rowsCnt / ITEMS_PER_PAGE - 1;
        } else {
            result = rowsCnt / ITEMS_PER_PAGE;
        }
        return result;
    }

    @Override
    public Long countCurrentPage(Long currentPage) {
        return currentPage;
    }


}
