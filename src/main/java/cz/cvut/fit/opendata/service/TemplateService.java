package cz.cvut.fit.opendata.service;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Service provides utilities for convenient filling templates for FE views with needed objects
 */
@Service
public interface TemplateService {

    String NEXT_PAGE = "nextPage";
    String PREVIOUS_PAGE = "previousPage";
    String FIRST_PAGE = "firstPage";
    String LAST_PAGE = "lastPage";
    String CURRENT_PAGE = "currentPage";
    String NEXT_PAGE_NUMBER = "nextPageNumber";
    String PREVIOUS_PAGE_NUMBER = "previousPageNumber";
    String FIRST_PAGE_NUMBER = "firstPageNumber";
    String LAST_PAGE_NUMBER = "lastPageNumber";
    String CURRENT_PAGE_NUMBER = "currentPageNumber";

    String STATIC_HOME_PAGE = "home";
    String STATIC_DOCUMENTS_PAGE = "documents";
    String STATIC_ORGANIZATION_PAGE = "organizations";
    String STATIC_API_PAGE = "api";
    String STATIC_ABOUT_PAGE = "about";

    String STATIC_API_DOCUMENTS = "api/documents";
    String STATIC_API_ORGANIZATIONS = "api/organizations";

    String HOME_PAGE_LINK = "homeLink";
    String DOCUMENTS_PAGE_LINK = "recordsLink";
    String ORGANIZATION_PAGE_LINK = "organizationsLink";
    String API_PAGE_LINK = "apiLink";
    String ABOUT_PAGE_LINK = "aboutLink";
    String BASE_PAGE_LINK = "baseLink";

    String API_DOCUMENTS_PAGE_LINK = "apiDocumentsLink";
    String API_ORGANIZATIONS_PAGE_LINK = "apiOrganizationsLink";


    /**
     * Method fills template with base links and pagination links
     *
     * @param modelAndView - ModelAndView to fill
     * @param request      - current request
     * @param templateName - name of view ot generate
     * @param currentPage  - number of current page of request
     * @param rowsCnt      - total number of rows in DB for this request
     */
    void fillTemplateBaseLinks(ModelAndView modelAndView, HttpServletRequest
            request, String templateName, Long currentPage, Long rowsCnt);

    /**
     * Method fills template with pagination links
     *
     * @param modelAndView - ModelAndView to fill
     * @param request      - current request
     * @param currentPage  - number current page of request
     * @param rowsCnt      - total number of rows in DB for this request
     */
    void fillTemplateWithPagination(ModelAndView modelAndView, HttpServletRequest request,
                                    Long currentPage, Long rowsCnt);

    /**
     * Method fills template with base links (navigation bar links, api links)
     *
     * @param modelAndView - ModelAndView to fill
     * @param request      - current request
     */
    void fillTemplateHeaderLinks(ModelAndView modelAndView, HttpServletRequest request);

}
