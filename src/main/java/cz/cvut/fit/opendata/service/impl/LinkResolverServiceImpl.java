package cz.cvut.fit.opendata.service.impl;

import cz.cvut.fit.opendata.service.LinkResolverService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Component
public class LinkResolverServiceImpl implements LinkResolverService {

    @Override
    public URI getPageUri(ServletUriComponentsBuilder builder, Long pageNumber) {
        builder.replaceQueryParam("page", pageNumber);
        return builder.build().toUri();
    }

    @Override
    public URI getPageUriReplacedPath(ServletUriComponentsBuilder builder, String replacedPath) {
        builder.replaceQuery(null);
        builder.replacePath(replacedPath);
        return builder.build().toUri();
    }
}
