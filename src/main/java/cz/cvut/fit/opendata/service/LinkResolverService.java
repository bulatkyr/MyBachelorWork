package cz.cvut.fit.opendata.service;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * Service provides utilities for resolving and building links
 */
@Service
public interface LinkResolverService {

    /**
     * Method changes value of query parameter "page" to pageNumber. Method is used for building pagination links.
     *
     * @param builder    - UriBuilder with current request loaded
     * @param pageNumber - new pageNumber
     * @return - new URI with specified pageNumber value
     */
    URI getPageUri(ServletUriComponentsBuilder builder, Long pageNumber);

    /**
     * Method changes path in current requested URL. Method is used for building navigation bar links
     *
     * @param builder      - UriBuilder with current request loaded
     * @param replacedPath - new path to replace
     * @return - new URI with replaced path
     */
    URI getPageUriReplacedPath(ServletUriComponentsBuilder builder, String replacedPath);

}
