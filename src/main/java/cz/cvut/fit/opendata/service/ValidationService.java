package cz.cvut.fit.opendata.service;

import cz.cvut.fit.opendata.model.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service provides utilities for all types of validation and parsing values related to business logic of application
 */
@Service
public interface ValidationService {

    /**
     * Method validates number on negative value.
     *
     * @param id - number to validate
     * @throws ValidationException - in case of negative number
     */
    void validateNonNegativeLong(Long id) throws ValidationException;

    /**
     * Method validates record type among record types in enum {@link cz.cvut.fit.opendata.model.RecordType}
     *
     * @param recordType - string to validate
     * @throws ValidationException - in case of record type is not found among allowed values
     */
    void validateRecordType(String recordType) throws ValidationException;

    /**
     * Method parse record types from request and validates each of them
     *
     * @param recordTypes - string to parse and validate
     * @return - parsed and validated record types
     * @throws ValidationException - in case of format of record types is not correct or any of record types fails to validate
     */
    List<String> parseRecordTypes(String recordTypes) throws ValidationException;

    /**
     * Method parse entity types from request and validates each of them
     *
     * @param entityTypes - string to parse and validate
     * @return - parsed and validated entity types
     * @throws ValidationException - in case of format of entity types is not correct or any of entity types fails to validate
     */
    List<String> parseEntityTypes(String entityTypes) throws ValidationException;

    /**
     * Method validates entity type among entity types in enum {@link cz.cvut.fit.opendata.model.EntityType}
     *
     * @param entityType - string to validate
     * @throws ValidationException - in case of entity type is not found among allowed values
     */
    void validateEntityType(String entityType) throws ValidationException;

    /**
     * Method validate ico according to rules of possible values of ico in Czech republic
     *
     * @param ico - string to validate
     * @return - validate ico and filled with zero on start if necessary
     */
    String validateIco(String ico) throws ValidationException;

    /**
     * Method validate correct numbers for start and end rows in request
     *
     * @param startRow - first number to validate
     * @param endRow   - second number to validate
     * @throws ValidationException - in case of end row is less the start row, or null values of rows
     */
    void validateStartEndRow(Long startRow, Long endRow) throws ValidationException;
}
