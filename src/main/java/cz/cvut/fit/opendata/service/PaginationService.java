package cz.cvut.fit.opendata.service;

import cz.cvut.fit.opendata.model.exception.ValidationException;
import org.springframework.stereotype.Service;

/**
 * Service provides utilities for pagination in application
 */
@Service
public interface PaginationService {

    int ITEMS_PER_PAGE = 10;

    /**
     * Validate current page number according to number of allowed rows
     *
     * @param currentPage - number of current page to validate
     * @param rowsCnt     - number of all rows in DB for this request
     * @throws ValidationException - in case of invalid current page
     */
    void validateCurrentPage(Long currentPage, Long rowsCnt) throws ValidationException;

    /**
     * Method counts number of next page according to current page and total number of rows in DB for this request
     *
     * @param currentPage - number of current page
     * @param rowsCnt     - total number of rows in DB for this request
     * @return - number of next page
     */
    Long countNextPage(Long currentPage, Long rowsCnt);

    /**
     * Method counts number of previous page according to current page and total number of rows in DB for this request
     *
     * @param currentPage - number of current page
     * @param rowsCnt     - total number of rows in DB for this request
     * @return - number of previous page
     */
    Long countPreviousPage(Long currentPage, Long rowsCnt);

    /**
     * Method counts start row for DB select according to current page and total number of rows in DB for this request
     *
     * @param currentPage - number of current page
     * @param rowsCnt     - total number of rows in DB for this request
     * @return - number of start row
     */
    Long countStartRow(Long currentPage, Long rowsCnt);

    /**
     * Method counts end row for DB select according to current page and total number of rows in DB for this request
     *
     * @param currentPage - number of current page
     * @param rowsCnt     - total number of rows in DB for this request
     * @return - number of end row
     */
    Long countEndRow(Long currentPage, Long rowsCnt);

    /**
     * Method counts first page
     *
     * @return - number of first page
     */
    @SuppressWarnings("SameReturnValue")
    Long countFirstPage();

    /**
     * Method counts last page according to total number of rows in DB for this type of request
     *
     * @param rowsCnt - total number of rows in DB for this request
     * @return - number of last page
     */
    Long countLastPage(Long rowsCnt);

    /**
     * Method counts current page.
     *
     * @param currentPage - number of current page
     * @return - number of current page
     */
    Long countCurrentPage(Long currentPage);
}
