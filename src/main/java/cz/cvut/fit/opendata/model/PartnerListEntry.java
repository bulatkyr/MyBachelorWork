package cz.cvut.fit.opendata.model;

import java.util.Objects;

/**
 * Class represents list of connected entities by document.
 * Currently not used in application ,added foe integrity with database.
 */
class PartnerListEntry {

    /**
     * Id and primary key
     */
    private Long partnerListEntryId;

    /**
     * Unique code of pair of entities
     */
    private String code;

    /**
     * Pointer to partner
     */
    private Entity partner;

    /**
     * Pointer to authority
     */
    private Entity authority;

    public PartnerListEntry() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartnerListEntry that = (PartnerListEntry) o;
        return Objects.equals(partnerListEntryId, that.partnerListEntryId) &&
                Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {

        return Objects.hash(partnerListEntryId, code);
    }

    public Long getPartnerListEntryId() {
        return partnerListEntryId;
    }

    public void setPartnerListEntryId(Long partnerListEntryId) {
        this.partnerListEntryId = partnerListEntryId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Entity getPartner() {
        return partner;
    }

    public void setPartner(Entity partner) {
        this.partner = partner;
    }

    public Entity getAuthority() {
        return authority;
    }

    public void setAuthority(Entity authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "PartnerListEntry{" +
                "partnerListEntryId=" + partnerListEntryId +
                ", code='" + code + '\'' +
                ", partner=" + partner +
                ", authority=" + authority +
                '}';
    }
}
