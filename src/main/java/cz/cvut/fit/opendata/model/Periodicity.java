package cz.cvut.fit.opendata.model;

/**
 * Enum represents possible values for attribute periodicity.
 * Periodicity is used in processed data instances, data sources, retrievals itself.
 * Represents periodicity for processing documents, for processing data sources, for payments in document.
 */
public enum Periodicity {

    daily("daily"),
    weekly("weekly"),
    monthly("monthly"),
    quarterly("quarterly"),
    yearly("yearly"),
    aperiodic("aperiodic")
    ;

    private final String periodicity;

    Periodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public String getPeriodicity() {
        return periodicity;
    }
}
