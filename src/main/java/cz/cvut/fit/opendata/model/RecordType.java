package cz.cvut.fit.opendata.model;

/**
 * Enum represents possible types of record(Document)
 * Currently used types are INVOICE, ORDER, CONTRACT, PAYMENT
 */
public enum RecordType {

    invoice("invoice"),
    order("order"),
    contract("contract"),
    payment("payment")
    ;

    private final String type;

    RecordType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
