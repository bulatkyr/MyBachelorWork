package cz.cvut.fit.opendata.model;

import java.util.Date;
import java.util.Objects;

/**
 * Class represents retrieval of the data instance in application for collecting data.
 * Objects of this class hold information about processing data instance.
 * It works as meta data holder and log holder of processing data instances.
 */
public class Retrieval {

    /**
     * Id and primary key
     */
    private Long retrievalId;

    /**
     * Date when retrieval was realized
     */
    private Date date;

    /**
     * In case of failure - description of failure of processing
     */
    private String failureReason;

    /**
     * Number of bad records collected during the processing data instance
     */
    private Integer numberBadRecords;

    /**
     * Number of good records collected during processing data instance
     */
    private Integer numberRecordsInserted;

    /**
     * Indicates whether processing of data instance successeded
     */
    private Boolean success;

    /**
     * Pointer to data instance that was processed by this retrieval
     */
    private DataInstance dataInstance;

    /**
     * Default constructor
     */
    public Retrieval() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Retrieval retrieval = (Retrieval) o;
        return Objects.equals(retrievalId, retrieval.retrievalId) &&
                Objects.equals(numberBadRecords, retrieval.numberBadRecords) &&
                Objects.equals(numberRecordsInserted, retrieval.numberRecordsInserted) &&
                Objects.equals(success, retrieval.success);
    }

    @Override
    public int hashCode() {

        return Objects.hash(retrievalId, numberBadRecords, numberRecordsInserted, success);
    }

    public Long getRetrievalId() {
        return retrievalId;
    }

    public void setRetrievalId(Long retrievalId) {
        this.retrievalId = retrievalId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public Integer getNumberBadRecords() {
        return numberBadRecords;
    }

    public void setNumberBadRecords(Integer numberBadRecords) {
        this.numberBadRecords = numberBadRecords;
    }

    public Integer getNumberRecordsInserted() {
        return numberRecordsInserted;
    }

    public void setNumberRecordsInserted(Integer numberRecordsInserted) {
        this.numberRecordsInserted = numberRecordsInserted;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public DataInstance getDataInstance() {
        return dataInstance;
    }

    public void setDataInstance(DataInstance dataInstance) {
        this.dataInstance = dataInstance;
    }

    @Override
    public String toString() {
        return "Retrieval{" +
                "retrievalId=" + retrievalId +
                ", date=" + date +
                ", failureReason='" + failureReason + '\'' +
                ", numberBadRecords=" + numberBadRecords +
                ", numberRecordsInserted=" + numberRecordsInserted +
                ", success=" + success +
                ", dataInstance=" + dataInstance +
                '}';
    }
}
