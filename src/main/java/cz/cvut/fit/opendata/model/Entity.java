package cz.cvut.fit.opendata.model;

import java.util.List;
import java.util.Objects;

/**
 * Class represents organization in our application.
 * Class holds all information about organizations that was collected from data instances.
 * Class represents publishing ministries, companies, individual businessman.
 */
public class Entity {

    /**
     * Id and primary key
     */
    private Long id;

    /**
     * DIČ - tax identification number of organization (VAT ID).
     */
    private String dic;

    /**
     * IČO - unique identification number of organization.
     */
    private String ico;

    /**
     * Name of the organization
     */
    private String name;

    /**
     * Indicates whether entity is public and publishing organization
     */
    private Boolean isPublic;

    /**
     * Type of entity (ministry, company, individual, other)
     */
    private EntityType entityType;

    /**
     * Records(Documents) that were published by this authority
     */
    private List<Record> recordsAsAuthority;

    /**
     * Records(Documents) that were signed by this entity. Other side of publishing authority
     */
    private List<Record> recordsAsPartner;

    /**
     * Default constructor
     */
    public Entity() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return Objects.equals(id, entity.id) &&
                Objects.equals(ico, entity.ico) &&
                Objects.equals(name, entity.name) &&
                entityType == entity.entityType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, ico, name, entityType);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDic() {
        return dic;
    }

    public void setDic(String dic) {
        this.dic = dic;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public List<Record> getRecordsAsAuthority() {
        return recordsAsAuthority;
    }

    public void setRecordsAsAuthority(List<Record> recordsAsAuthority) {
        this.recordsAsAuthority = recordsAsAuthority;
    }

    public List<Record> getRecordsAsPartner() {
        return recordsAsPartner;
    }

    public void setRecordsAsPartner(List<Record> recordsAsPartner) {
        this.recordsAsPartner = recordsAsPartner;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                ", dic='" + dic + '\'' +
                ", ico='" + ico + '\'' +
                ", name='" + name + '\'' +
                ", isPublic=" + isPublic +
                ", entityType=" + entityType +
                ", recordsAsAuthority=" + recordsAsAuthority +
                ", recordsAsPartner=" + recordsAsPartner +
                '}';
    }
}
