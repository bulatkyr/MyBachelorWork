package cz.cvut.fit.opendata.model;

import java.util.Date;
import java.util.Objects;

/**
 * Core class Record represents document in application.
 * Record holds all information about document that were collected from processed data instances.
 */
public class Record {

    /**
     * Od and primary key
     */
    private Long recordId;

    /**
     * Amount of the record in CZK
     */
    private Double amountCZK;

    /**
     * Identifier of this record given by publishing authority
     */
    private String authorityIdentifier;

    /**
     * Currency that is used in record
     */
    private String currency;

    /**
     * Date of creation document that is represented by this record
     */
    private Date dateCreated;

    /**
     * Expiry date of document that is represented by this record
     */
    private Date dateOfExpiry;

    /**
     * Date of payment that is given in document represented by this record
     */
    private Date dateOfPayment;

    /**
     * Due date of the record
     */
    private Date dueDate;

    /**
     * Indicates whether contract is in effect (updated after each processing)
     */
    private Boolean inEffect;

    /**
     * Attribute to identify correlated records for the same transaction but published by different organizations
     */
    private String masterId;

    /**
     * Amount of the record in original currency
     */
    private Double originalCurrencyAmount;

    /**
     * Description of the document
     */
    private String subject;

    /**
     * Variable symbol. Used in invoice or order types of record
     */
    private String variableSymbol;

    /**
     * Pointer to partner organization(other than publishing side)
     */
    private Entity partner;

    /**
     * Pointer to publishing organization of this record
     */
    private Entity authority;

    /**
     * Pointer to parent record if there is any connected
     */
    private Record parentRecord;

    /**
     * Pointer to retrieval in which this record was collected
     */
    private Retrieval retrieval;

    /**
     * Type of the record
     */
    private RecordType recordType;

    /**
     * Type of the authority in this document
     */
    private AuthorityRole authorityRole;

    /**
     * More general description for record
     */
    private String budgetCategory;

    /**
     * Periodicity of recurring payments
     */
    private Periodicity periodicity;

    /**
     * Default constructor
     */
    public Record() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return Objects.equals(recordId, record.recordId) &&
                Objects.equals(amountCZK, record.amountCZK) &&
                Objects.equals(currency, record.currency) &&
                Objects.equals(inEffect, record.inEffect) &&
                Objects.equals(masterId, record.masterId) &&
                Objects.equals(originalCurrencyAmount, record.originalCurrencyAmount) &&
                recordType == record.recordType &&
                authorityRole == record.authorityRole &&
                periodicity == record.periodicity;
    }

    @Override
    public int hashCode() {

        return Objects.hash(recordId, amountCZK, currency, inEffect, masterId, originalCurrencyAmount, recordType, authorityRole, periodicity);
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Double getAmountCZK() {
        return amountCZK;
    }

    public void setAmountCZK(Double amountCZK) {
        this.amountCZK = amountCZK;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getauthorityIdentifier() {
        return authorityIdentifier;
    }

    public void setauthorityIdentifier(String authorityIdentifier) {
        this.authorityIdentifier = authorityIdentifier;
    }

    public Date getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean getInEffect() {
        return inEffect;
    }

    public void setInEffect(Boolean inEffect) {
        this.inEffect = inEffect;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public Double getOriginalCurrencyAmount() {
        return originalCurrencyAmount;
    }

    public void setOriginalCurrencyAmount(Double originalCurrencyAmount) {
        this.originalCurrencyAmount = originalCurrencyAmount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(String variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public Entity getPartner() {
        return partner;
    }

    public void setPartner(Entity partner) {
        this.partner = partner;
    }

    public Entity getAuthority() {
        return authority;
    }

    public void setAuthority(Entity authority) {
        this.authority = authority;
    }

    public Record getParentRecord() {
        return parentRecord;
    }

    public void setParentRecord(Record parentRecord) {
        this.parentRecord = parentRecord;
    }

    public Retrieval getRetrieval() {
        return retrieval;
    }

    public void setRetrieval(Retrieval retrieval) {
        this.retrieval = retrieval;
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public AuthorityRole getAuthorityRole() {
        return authorityRole;
    }

    public void setAuthorityRole(AuthorityRole authorityRole) {
        this.authorityRole = authorityRole;
    }

    public String getBudgetCategory() {
        return budgetCategory;
    }

    public void setBudgetCategory(String budgetCategory) {
        this.budgetCategory = budgetCategory;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    @Override
    public String toString() {
        return "Record{" +
                "recordId=" + recordId +
                ", amountCZK=" + amountCZK +
                ", authorityIdentifier='" + authorityIdentifier + '\'' +
                ", currency='" + currency + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateOfExpiry=" + dateOfExpiry +
                ", dateOfPayment=" + dateOfPayment +
                ", dueDate=" + dueDate +
                ", inEffect=" + inEffect +
                ", masterId='" + masterId + '\'' +
                ", originalCurrencyAmount=" + originalCurrencyAmount +
                ", subject='" + subject + '\'' +
                ", variableSymbol='" + variableSymbol + '\'' +
                ", partner=" + partner +
                ", authority=" + authority +
                ", parentRecord=" + parentRecord +
                ", retrieval=" + retrieval +
                ", recordType=" + recordType +
                ", authorityRole=" + authorityRole +
                ", budgetCategory='" + budgetCategory + '\'' +
                ", periodicity=" + periodicity +
                '}';
    }
}
