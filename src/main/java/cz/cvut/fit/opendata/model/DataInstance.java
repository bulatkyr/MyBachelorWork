package cz.cvut.fit.opendata.model;

import java.util.Date;
import java.util.Objects;

/**
 * Class DataInstance represents a file with data to be processed.
 * Data instances can have different formats.
 * For each data instance there is special mapping file that help to process data and map them to retrieval.
 * Data instances are connected to data source. one data source can have more than one data instance.
 */
public class DataInstance {

    /**
     * Id and primary key
     */
    private Long dataInstanceId;

    /**
     * Format of data instance. (xls, xlsx, xml...)
     */
    private String format;

    /**
     * URL to download this data instance
     */
    private String url;

    /**
     * Foreign key to DataSource to which data instance is connected
     */
    private DataSource dataSource;

    /**
     * Periodicity of updates the file for this data instance
     */
    private Periodicity periodicity;

    /**
     * Last time when data instance was processed
     */
    private Date lastProcessedDate;

    /**
     * The expiry date for this data instance
     */
    private Date expires;

    /**
     * Last row that was processed in this data instance
     */
    private Integer lastProcessedRow;

    /**
     * The identifier that used by authority for this data instance
     */
    private String authorityId;

    /**
     * Description of this data instance
     */
    private String description;

    /**
     * Name of the document that is used to process and map data from this data instance
     */
    private String mappingFile;

    /**
     * Indicates whether file was processed once and during next retrievals processing can start from last processed row
     */
    private Boolean incremental;

    /**
     * Default constructor
     */
    public DataInstance() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataInstance that = (DataInstance) o;
        return Objects.equals(dataInstanceId, that.dataInstanceId) &&
                Objects.equals(format, that.format) &&
                Objects.equals(url, that.url) &&
                periodicity == that.periodicity &&
                Objects.equals(mappingFile, that.mappingFile);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dataInstanceId, format, url, periodicity, mappingFile);
    }

    public Long getDataInstanceId() {
        return dataInstanceId;
    }

    public void setDataInstanceId(Long dataInstanceId) {
        this.dataInstanceId = dataInstanceId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public Date getLastProcessedDate() {
        return lastProcessedDate;
    }

    public void setLastProcessedDate(Date lastProcessedDate) {
        this.lastProcessedDate = lastProcessedDate;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public Integer getLastProcessedRow() {
        return lastProcessedRow;
    }

    public void setLastProcessedRow(Integer lastProcessedRow) {
        this.lastProcessedRow = lastProcessedRow;
    }

    public String getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMappingFile() {
        return mappingFile;
    }

    public void setMappingFile(String mappingFile) {
        this.mappingFile = mappingFile;
    }

    public Boolean getIncremental() {
        return incremental;
    }

    public void setIncremental(Boolean incremental) {
        this.incremental = incremental;
    }

    @Override
    public String toString() {
        return "DataInstance{" +
                "dataInstanceId=" + dataInstanceId +
                ", format='" + format + '\'' +
                ", url='" + url + '\'' +
                ", dataSource=" + dataSource +
                ", periodicity=" + periodicity +
                ", lastProcessedDate=" + lastProcessedDate +
                ", expires=" + expires +
                ", lastProcessedRow=" + lastProcessedRow +
                ", authorityId='" + authorityId + '\'' +
                ", description='" + description + '\'' +
                ", mappingFile='" + mappingFile + '\'' +
                ", incremental=" + incremental +
                '}';
    }
}
