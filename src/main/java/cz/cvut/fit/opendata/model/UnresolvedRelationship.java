package cz.cvut.fit.opendata.model;

import java.util.Objects;

/**
 * Class represent unresolved relationships that were discovered during collecting data from data instances.
 * Currently not used in web application, only by application for collecting data.
 */
class UnresolvedRelationship {

    /**
     * Id and primary key
     */
    private Long unresolvedRelationshipId;

    /**
     * Type of unresolved record
     */
    private RecordType recordType;

    /**
     * Indicates whether saved record is parent of another record
     */
    private Boolean savedRecordIsParent;

    /**
     * Id of saved record
     */
    private Long savedRecordId;

    /**
     * Identifier of publishing authority of saved record
     */
    private String boundAuthorityIdentifier;

    /**
     * Pointer to record
     */
    private Record recordId;

    /**
     * Default constructor
     */
    public UnresolvedRelationship() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnresolvedRelationship that = (UnresolvedRelationship) o;
        return Objects.equals(unresolvedRelationshipId, that.unresolvedRelationshipId) &&
                recordType == that.recordType &&
                Objects.equals(savedRecordId, that.savedRecordId) &&
                Objects.equals(boundAuthorityIdentifier, that.boundAuthorityIdentifier);
    }

    @Override
    public int hashCode() {

        return Objects.hash(unresolvedRelationshipId, recordType, savedRecordId, boundAuthorityIdentifier);
    }

    public Long getUnresolvedRelationshipId() {
        return unresolvedRelationshipId;
    }

    public void setUnresolvedRelationshipId(Long unresolvedRelationshipId) {
        this.unresolvedRelationshipId = unresolvedRelationshipId;
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public Boolean getSavedRecordIsParent() {
        return savedRecordIsParent;
    }

    public void setSavedRecordIsParent(Boolean savedRecordIsParent) {
        this.savedRecordIsParent = savedRecordIsParent;
    }

    public Long getSavedRecordId() {
        return savedRecordId;
    }

    public void setSavedRecordId(Long savedRecordId) {
        this.savedRecordId = savedRecordId;
    }

    public String getBoundAuthorityIdentifier() {
        return boundAuthorityIdentifier;
    }

    public void setBoundAuthorityIdentifier(String boundAuthorityIdentifier) {
        this.boundAuthorityIdentifier = boundAuthorityIdentifier;
    }

    public Record getRecordId() {
        return recordId;
    }

    public void setRecordId(Record recordId) {
        this.recordId = recordId;
    }

    @Override
    public String toString() {
        return "UnresolvedRelationship{" +
                "unresolvedRelationshipId=" + unresolvedRelationshipId +
                ", recordType=" + recordType +
                ", savedRecordIsParent=" + savedRecordIsParent +
                ", savedRecordId=" + savedRecordId +
                ", boundAuthorityIdentifier='" + boundAuthorityIdentifier + '\'' +
                ", recordId=" + recordId +
                '}';
    }
}

