package cz.cvut.fit.opendata.model;

import java.util.Date;
import java.util.Objects;

/**
 * Class DataSource represents source of data in our application.
 * Class and table are used in database as bridge between file to processed and authority that this file publish.
 * Class holds meta data for correct work of application that collect data from authority
 */
public class DataSource {

    /**
     * Id and primary key
     */
    private Long dataSourceId;

    /**
     * Last time this data source was processed
     */
    private Date lastProcessedDate;

    /**
     * Pointer to authority that publish files to process
     */
    private Entity entity;

    /**
     * The type of records that are expected to be published with this data source
     */
    private String recordType;

    /**
     * Periodicity of publishing new files to process with this data source
     */
    private Periodicity periodicity;

    /**
     * Pointer to a class of application for collecting data which is responsible for handling data from this data source
     */
    private String handlingClass;

    /**
     * Indicates whether data source is active
     */
    private Boolean active;

    /**
     * Description of data source
     */
    private String description;

    /**
     * Default constructor
     */
    public DataSource() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataSource that = (DataSource) o;
        return Objects.equals(dataSourceId, that.dataSourceId) &&
                periodicity == that.periodicity &&
                Objects.equals(handlingClass, that.handlingClass) &&
                Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dataSourceId, periodicity, handlingClass, active);
    }

    public Long getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(Long dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public Date getLastProcessedDate() {
        return lastProcessedDate;
    }

    public void setLastProcessedDate(Date lastProcessedDate) {
        this.lastProcessedDate = lastProcessedDate;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public String getHandlingClass() {
        return handlingClass;
    }

    public void setHandlingClass(String handlingClass) {
        this.handlingClass = handlingClass;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "DataSource{" +
                "dataSourceId=" + dataSourceId +
                ", lastProcessedDate=" + lastProcessedDate +
                ", entity=" + entity +
                ", recordType='" + recordType + '\'' +
                ", periodicity=" + periodicity +
                ", handlingClass='" + handlingClass + '\'' +
                ", active=" + active +
                ", description='" + description + '\'' +
                '}';
    }
}
