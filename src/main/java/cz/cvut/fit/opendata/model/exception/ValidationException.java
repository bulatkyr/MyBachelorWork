package cz.cvut.fit.opendata.model.exception;

public class ValidationException extends Exception {

    private String mapperLogMessage;
    private String userDescriptionMessage;

    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    public String getMapperLogMessage() {
        return mapperLogMessage;
    }

    public void setMapperLogMessage(String mapperLogMessage) {
        this.mapperLogMessage = mapperLogMessage;
    }

    public String getUserDescriptionMessage() {
        return userDescriptionMessage;
    }

    public void setUserDescriptionMessage(String userDescriptionMessage) {
        this.userDescriptionMessage = userDescriptionMessage;
    }
}
