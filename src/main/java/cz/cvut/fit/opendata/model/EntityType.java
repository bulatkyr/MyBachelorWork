package cz.cvut.fit.opendata.model;

/**
 * Enum represents values for possible types of organization
 * Currently there are registered four possible types
 */
public enum EntityType {

    ministry("ministry"),
    company("company"),
    individual("individual"),
    other("other");

    private final String type;

    EntityType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
