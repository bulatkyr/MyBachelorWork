package cz.cvut.fit.opendata.model;

/**
 * Enum represents possible roles for entity relatively another entity in the collected document.
 * Current possible values are SUPPLIER and CUSTOMER
 */
public enum AuthorityRole {
    supplier("supplier"),
    customer("customer");

    private final String role;

    AuthorityRole(String role) {
        this.role = role;
    }

    public String getAuthorityRole() {
        return this.role;
    }
}
