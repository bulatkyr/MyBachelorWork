package cz.cvut.fit.opendata.mapper;


import cz.cvut.fit.opendata.model.Record;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Interface for mapping request to DB related to documents and organization connected to this documents
 */
@Mapper
public interface RecordDataMapper {

    /**
     * Method selects total number of documents for requested document types
     *
     * @param recordTypes    - requested document types
     * @param organizationId - requested organization id
     * @return - number of documents that fit to requested data
     */
    Long getRecordsCountByTypes(@Param("recordTypes") List<String> recordTypes,
                                @Param("organizationId") Long organizationId);

    /**
     * Method selects base information of each document between requested rows, requested types of organization and requested organization id
     *
     * @param rowNumStart    - first number of row to select
     * @param rowNumEnd      - last number of row select
     * @param recordTypes    - types of documents to select
     * @param organizationId - organization id that document id connected to
     * @return - list of documents that fir to requested data
     */
    List<Record> getAllRecordsShortBetweenRowsByTypes(@Param("rowNumStart") Long rowNumStart,
                                                      @Param("rowNumEnd") Long rowNumEnd,
                                                      @Param("recordTypes") List<String> recordTypes,
                                                      @Param("organizationId") Long organizationId);

    /**
     * Method selects detailed information about document by it's id
     *
     * @param id - id of requested document
     * @return - requested document
     */
    Record getRecordById(@Param("id") Long id);

    /**
     * Method selects full information about documents between requested rows and document types
     *
     * @param rowNumStart - first number of row to select
     * @param rowNumEnd   - last number of row to select
     * @param recordTypes - types of documents to select
     * @return - list of documents with full information
     */
    List<Record> getAllRecordsFullBetweenRowsByTypes(@Param("rowNumStart") Long rowNumStart,
                                                     @Param("rowNumEnd") Long rowNumEnd,
                                                     @Param("recordTypes") List<String> recordTypes);
}
