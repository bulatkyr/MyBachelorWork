package cz.cvut.fit.opendata.mapper;

import cz.cvut.fit.opendata.model.Entity;
import cz.cvut.fit.opendata.model.Record;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Interface for mapping request to DB related to organization and documents connected to this organization
 */
@Mapper
public interface EntityDataMapper {

    /**
     * Method selects total number organizations for requested organization types
     *
     * @param entityTypes - requested organization types
     * @return - number of organization available for this request
     */
    Long getEntitiesCountByTypes(@Param("entityTypes") List<String> entityTypes);

    /**
     * Method selects base information of each organization between requested rows and requested types of organization
     *
     * @param rowNumStart - first number of row to select
     * @param rowNumEnd   - last number of row select
     * @param entityTypes - types of organization to select
     * @return - list of organization with base information
     */
    List<Entity> getAllEntitiesShortBetweenRowsByTypes(@Param("rowNumStart") Long rowNumStart,
                                                       @Param("rowNumEnd") Long rowNumEnd,
                                                       @Param("entityTypes") List<String> entityTypes);

    /**
     * Method selects full information of organizations between requested rows and requested types of organization
     *
     * @param rowNumStart - first number of row to select
     * @param rowNumEnd   - last number of row to select
     * @param entityTypes - types of organization to select
     * @return - list of organization with full information
     */
    List<Entity> getAllEntitiesFullBetweenRowsByTypes(@Param("rowNumStart") Long rowNumStart,
                                                      @Param("rowNumEnd") Long rowNumEnd,
                                                      @Param("entityTypes") List<String> entityTypes);

    /**
     * Method select detailed infor about requested organization
     *
     * @param entityId - id of organization to select
     * @return - select organization if exists with this id
     */
    Entity getEntityDetailById(@Param("entityId") Long entityId);

    /**
     * Method selects documents as partner for requested entity
     *
     * @param entityId - id of entity, which documents are requested
     * @return - list of documents
     */
    List<Record> getRecordsAsPartnerByEntityId(@Param("entityId") Long entityId);

    /**
     * Method selects detailed information about organization by it's ICO
     *
     * @param ico - ICO of organization
     * @return - selected organization
     */
    Entity getEntityByIco(@Param("ico") String ico);
}
