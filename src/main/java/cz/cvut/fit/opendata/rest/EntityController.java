package cz.cvut.fit.opendata.rest;

import cz.cvut.fit.opendata.mapper.EntityDataMapper;
import cz.cvut.fit.opendata.model.Entity;
import cz.cvut.fit.opendata.model.exception.ValidationException;
import cz.cvut.fit.opendata.service.PaginationService;
import cz.cvut.fit.opendata.service.TemplateService;
import cz.cvut.fit.opendata.service.ValidationService;
import cz.cvut.fit.opendata.service.impl.PaginationServiceImpl;
import cz.cvut.fit.opendata.service.impl.TemplateServiceImpl;
import cz.cvut.fit.opendata.service.impl.ValidationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Spring controller component with mapped methods for actions related to organizations ({@link cz.cvut.fit.opendata.model.Entity}) in application
 */

@RestController
public class EntityController {

    private static final Logger log = LoggerFactory.getLogger(EntityController.class);

    private static final List<String> entityTypesFull = Arrays.asList("company", "ministry", "individual", "other");

    final private EntityDataMapper entityDataMapper;
    final private PaginationService paginationService;
    final private TemplateService templateService;
    final private ValidationService validationService;

    public EntityController(EntityDataMapper entityDataMapper, PaginationServiceImpl paginationService,
                            TemplateServiceImpl templateService, ValidationServiceImpl validationService) {
        this.entityDataMapper = entityDataMapper;
        this.paginationService = paginationService;
        this.templateService = templateService;
        this.validationService = validationService;
    }

    /*----------------------------API PART-----------------------------------------*/

    /**
     * Method for api requests on organizations in application DB. Request can be parametrized with number of rows and types of organization
     *
     * @param rowStart            - first number of organization to select
     * @param rowEnd              - last number of organization to select
     * @param organizationTypesIn - types of organization to select
     * @param request             - processed client request
     * @return -    in case of success - returns list of requested organizations with 200 HTTP status,
     * in case of failure at validation - returns empty list with 400 HTTP status
     */
    @RequestMapping(value = "/api/organizations", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Entity>> getAllOrganizationsApi(
            @RequestParam(value = "rowStart", required = false, defaultValue = "0") Long rowStart,
            @RequestParam(value = "rowEnd", required = false, defaultValue = "10") Long rowEnd,
            @RequestParam(value = "organizationTypes", required = false) String organizationTypesIn,
            HttpServletRequest request
    ) {
        List<Entity> result = new ArrayList<>();
        List<String> entityTypes;
        try {
            entityTypes = new ArrayList<>(
                    organizationTypesIn == null ? entityTypesFull : validationService.parseEntityTypes(organizationTypesIn));
            validationService.validateStartEndRow(rowStart, rowEnd);
        } catch (ValidationException e) {
            log.error("Processing {} failed with parameters {}, exception massage {} ",
                    request.getRequestURL(), request.getQueryString(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        result.addAll(entityDataMapper.getAllEntitiesFullBetweenRowsByTypes(rowStart, rowEnd, entityTypes));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Method for api requests on detail of organization in application DB.
     * Request can be parametrized with ico of organization
     *
     * @param ico - ICO of requested organization
     * @return - in case of success - returns requested organization with 200 HTTP status,
     * in case of failure at validation - returns empty response with 400 HTTP status
     */
    @RequestMapping(value = "/api/organizations", params = {"ico"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Entity> getOrganizationByIcoApi(
            @RequestParam(value = "ico") String ico,
            HttpServletRequest request
    ) {
        Entity result = new Entity();
        String validatedIco;
        try {
            validatedIco = validationService.validateIco(ico);
        } catch (ValidationException e) {
            log.error("Processing {} failed with parameters {}, exception massage {} ",
                    request.getRequestURL(), request.getQueryString(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        result = entityDataMapper.getEntityByIco(validatedIco);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Method for api requests on detail of organization in application DB by it' ID.
     *
     * @param organizationId - id of requested organization
     * @param request        - processed client request
     * @return - in case of success - returns requested organization with 200 HTTP status,
     * in case of failure at validation - returns empty response with 400 HTTP status
     */
    @RequestMapping(value = "/api/organizations/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Entity> getOrganizationByIdApi(
            @PathVariable("id") Long organizationId,
            HttpServletRequest request
    ) {
        Entity result = new Entity();
        try {
            validationService.validateNonNegativeLong(organizationId);
        } catch (ValidationException e) {
            log.error("Processing {} failed with parameters {}, exception massage {} ",
                    request.getRequestURL(), request.getQueryString(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        result = entityDataMapper.getEntityDetailById(organizationId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    /*----------------------------API PART END-----------------------------------------*/

    /**
     * Method for web portal requests on list of organizations. Request can be parametrized with types of organizations and number of page
     *
     * @param pageNumber          - number of requested page
     * @param organizationTypesIn - types of organizations that are requested
     * @param request             - processed client request
     * @return - in case of success - returns {@link ModelAndView} with filled name of template, organizations and data to generate with 200 HTTP status
     * in case of validation failure - returns {@link ModelAndView} with error view and description of exception with 400 HTTP status
     * @throws ValidationException - in case of validation failure
     */
    @RequestMapping(value = "/organizations", method = RequestMethod.GET)
    public ModelAndView getAllOrganizations(
            @RequestParam(value = "page", required = false, defaultValue = "0") Long pageNumber,
            @RequestParam(value = "organizationTypes", required = false) String organizationTypesIn,
            HttpServletRequest request
    ) throws ValidationException {
        ModelAndView modelAndView = new ModelAndView();
        List<String> entityTypes = new ArrayList<>(
                organizationTypesIn == null ? entityTypesFull : validationService.parseEntityTypes(organizationTypesIn));
        Long cntRecords = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        paginationService.validateCurrentPage(pageNumber, cntRecords);
        List<Entity> result = new ArrayList<>(entityDataMapper.getAllEntitiesShortBetweenRowsByTypes(
                paginationService.countStartRow(pageNumber, cntRecords),
                paginationService.countEndRow(pageNumber, cntRecords),
                entityTypes));
        templateService.fillTemplateBaseLinks(modelAndView, request, "organizationsPage", pageNumber, cntRecords);
        if (organizationTypesIn != null) {
            for (String element : entityTypes) {
                modelAndView.addObject("entityType" + element, true);
            }
        }
        modelAndView.addObject("entities", result);
        modelAndView.addObject("entityDetailLink", request.getRequestURL());
        return modelAndView;
    }

    /**
     * Method for web portal requests on detail of the organization by it's ID
     *
     * @param organizationId - ID of requested organization
     * @param request        - processed client request
     * @return -  - in case of success - returns {@link ModelAndView} with filled name of template, requested organization and data to generate with 200 HTTP status
     * in case of validation failure - returns {@link ModelAndView} with error view and description of exception with 400 HTTP status
     * @throws ValidationException - in case of validation exception
     */
    @RequestMapping(value = "/organizations/{id}", method = RequestMethod.GET)
    public ModelAndView getOrganizationDetail(
            @PathVariable("id") Long organizationId,
            HttpServletRequest request
    ) throws ValidationException {
        ModelAndView modelAndView = new ModelAndView();
        validationService.validateNonNegativeLong(organizationId);
        Entity result = entityDataMapper.getEntityDetailById(organizationId);
        result.setRecordsAsPartner(entityDataMapper.getRecordsAsPartnerByEntityId(organizationId));
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("organizationDetailPage");
        modelAndView.addObject("entity", result);
        return modelAndView;
    }
}
