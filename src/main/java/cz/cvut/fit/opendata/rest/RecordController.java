package cz.cvut.fit.opendata.rest;

import cz.cvut.fit.opendata.mapper.RecordDataMapper;
import cz.cvut.fit.opendata.model.Record;
import cz.cvut.fit.opendata.model.exception.ValidationException;
import cz.cvut.fit.opendata.service.PaginationService;
import cz.cvut.fit.opendata.service.TemplateService;
import cz.cvut.fit.opendata.service.ValidationService;
import cz.cvut.fit.opendata.service.impl.PaginationServiceImpl;
import cz.cvut.fit.opendata.service.impl.TemplateServiceImpl;
import cz.cvut.fit.opendata.service.impl.ValidationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Spring controller component with mapped methods for actions related to documents ({@link cz.cvut.fit.opendata.model.Record}) in application
 */
@RestController
public class RecordController {

    private static final Logger log = LoggerFactory.getLogger(RecordController.class);

    final private RecordDataMapper mapper;

    final private PaginationService paginationService;

    final private TemplateService templateService;

    final private ValidationService validationService;

    private static final List<String> recordTypesFull = Arrays.asList("payment", "contract", "order", "invoice");

    public RecordController(RecordDataMapper recordDataMapper, PaginationServiceImpl paginationService,
                            TemplateServiceImpl templateService, ValidationServiceImpl validationService) {
        this.mapper = recordDataMapper;
        this.paginationService = paginationService;
        this.templateService = templateService;
        this.validationService = validationService;
    }

    /*-------------------------API PART-------------------------------------------*/

    /**
     * Method for api requests on documents in application DB. Count and document types can be controlled by parameters.
     *
     * @param rowStart      - first number of document to select
     * @param rowEnd        - last number of document to select
     * @param recordTypesIn - types of document to select
     * @param request       - processed request from client
     * @return -    in case of success - returns list of requested documents with 200 HTTP status,
     * in case of failure at validation - returns empty list with 400 HTTP status
     */
    @RequestMapping(value = "/api/documents", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Record>> getAllRecordsApi(
            @RequestParam(value = "rowStart", required = false, defaultValue = "0") Long rowStart,
            @RequestParam(value = "rowEnd", required = false, defaultValue = "10") Long rowEnd,
            @RequestParam(value = "documentTypes", required = false) String recordTypesIn,
            HttpServletRequest request
    ) {
        List<Record> result = new ArrayList<>();
        List<String> recordTypes;
        try {
            recordTypes = new ArrayList<>(
                    recordTypesIn == null ? recordTypesFull : validationService.parseRecordTypes(recordTypesIn));
            validationService.validateStartEndRow(rowStart, rowEnd);
        } catch (ValidationException e) {
            log.error("Processing {} failed with parameters {}, exception massage {} ",
                    request.getRequestURL(), request.getQueryString(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        result.addAll(mapper.getAllRecordsFullBetweenRowsByTypes(rowStart, rowEnd, recordTypes));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Method for api requests on detail of document in DB by it's ID.
     *
     * @param id      - id of requested document
     * @param request - processed request from client
     * @return - in case of success - returns requested document with 200 HTTP status,
     * in case of failure at validation - returns empty response with 400 HTTP status
     */
    @RequestMapping(value = "/api/documents/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Record> getRecordByIdApi(
            @PathVariable("id") Long id,
            HttpServletRequest request
    ) {
        Record result = new Record();
        try {
            validationService.validateNonNegativeLong(id);
        } catch (ValidationException e) {
            log.error("Processing {} failed with parameters {}, exception massage {} ",
                    request.getRequestURL(), request.getQueryString(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        result = mapper.getRecordById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /*-------------------------API PART END-------------------------------------------*/

    /**
     * Method for web portal requests on list of documents. Request can be parametrized with types of documents,
     * number of page and organization ID to which documents are connected
     *
     * @param pageNumber     - number of requested page
     * @param recordTypesIn  - types of documents that are requested
     * @param organizationId - ID of organization to which documents are connected
     * @param request        - processed client request
     * @return - in case of success - returns {@link ModelAndView} with filled name of template, documents and data to generate with 200 HTTP status
     * in case of validation failure - returns {@link ModelAndView} with error view and description of exception with 400 HTTP status
     * @throws ValidationException - in case of validation exception
     */
    @RequestMapping(value = "/documents", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView getAllRecords(
            @RequestParam(value = "page", required = false, defaultValue = "0") Long pageNumber,
            @RequestParam(value = "documentTypes", required = false) String recordTypesIn,
            @RequestParam(value = "organization", required = false) Long organizationId,
            HttpServletRequest request
    ) throws ValidationException {
        ModelAndView modelAndView = new ModelAndView();
        List<String> recordTypes = new ArrayList<>(
                recordTypesIn == null ? recordTypesFull : validationService.parseRecordTypes(recordTypesIn));
        Long cntRecords = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        paginationService.validateCurrentPage(pageNumber, cntRecords);
        List<Record> result = new ArrayList<>(mapper.getAllRecordsShortBetweenRowsByTypes(
                paginationService.countStartRow(pageNumber, cntRecords),
                paginationService.countEndRow(pageNumber, cntRecords),
                recordTypes, organizationId));
        templateService.fillTemplateBaseLinks(modelAndView, request, "recordsPage", pageNumber, cntRecords);
        if (recordTypesIn != null) {
            for (String element : recordTypes) {
                modelAndView.addObject("recordType" + element, true);
            }
        }
        modelAndView.addObject("records", result);
        modelAndView.addObject("recordDetailLink", request.getRequestURL());
        return modelAndView;
    }

    /**
     * Method for web portal requests on detail of the document by it's ID
     *
     * @param id      - ID of requested document
     * @param request - processed client request
     * @return -  - in case of success - returns {@link ModelAndView} with filled name of template, requested document and data to generate with 200 HTTP status
     * in case of validation failure - returns {@link ModelAndView} with error view and description of exception with 400 HTTP status
     * @throws ValidationException - in case of validation exception
     */
    @RequestMapping(value = "/documents/{id}", method = RequestMethod.GET)
    public ModelAndView getRecordById(
            @PathVariable("id") Long id,
            HttpServletRequest request
    ) throws ValidationException {
        ModelAndView modelAndView = new ModelAndView();
        validationService.validateNonNegativeLong(id);
        Record record = mapper.getRecordById(id);
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("recordDetailPage");
        modelAndView.addObject("record", record);
        return modelAndView;
    }
}
