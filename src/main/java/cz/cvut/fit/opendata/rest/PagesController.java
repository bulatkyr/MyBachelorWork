package cz.cvut.fit.opendata.rest;


import cz.cvut.fit.opendata.service.TemplateService;
import cz.cvut.fit.opendata.service.impl.TemplateServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
public class PagesController implements ErrorController {

    private static final Logger log = LoggerFactory.getLogger(PagesController.class);

    final private TemplateService templateService;

    private static final String ERROR_PATH = "/error";

    public PagesController(TemplateServiceImpl templateService) {
        this.templateService = templateService;
    }

    @RequestMapping(value = "/")
    public ModelAndView getDefaultPage(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @RequestMapping(value = "/home")
    public ModelAndView getHomePage(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @RequestMapping(value = "/api")
    public ModelAndView getApiPage(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("api");
        return modelAndView;
    }

    @RequestMapping(value = "/about")
    public ModelAndView getAboutPage(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("about");
        return modelAndView;
    }

    @RequestMapping(value = ERROR_PATH)
    public ModelAndView getErrorPage(HttpServletRequest request) {
        log.error("Processing {} failed with parameters {}",
                request.getRequestURL(), request.getQueryString());
        ModelAndView modelAndView = new ModelAndView();
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
