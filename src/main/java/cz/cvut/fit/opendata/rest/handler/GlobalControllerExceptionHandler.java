package cz.cvut.fit.opendata.rest.handler;

import cz.cvut.fit.opendata.model.exception.ValidationException;
import cz.cvut.fit.opendata.rest.RecordController;
import cz.cvut.fit.opendata.service.TemplateService;
import cz.cvut.fit.opendata.service.impl.TemplateServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(RecordController.class);
    private final TemplateService templateService;

    public GlobalControllerExceptionHandler(TemplateServiceImpl templateService) {
        this.templateService = templateService;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    public ModelAndView handleValidationException(HttpServletRequest request, ValidationException ex) {
        log.error("Processing {} failed with parameters {}, exception massage {} ",
                request.getRequestURL(), request.getQueryString(), ex.getMessage());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("validationError");
        return modelAndView;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(Exception.class)
    public ModelAndView handleCommonException(HttpServletRequest request) {
        log.error("Processing {} failed with parameters {}",
                request.getRequestURL(), request.getQueryString());
        ModelAndView modelAndView = new ModelAndView();
        templateService.fillTemplateHeaderLinks(modelAndView, request);
        modelAndView.setViewName("error404");
        return modelAndView;
    }

}
