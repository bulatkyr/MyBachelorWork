<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--suppress SqlResolve -->
<mapper namespace="cz.cvut.fit.opendata.mapper.RecordDataMapper">

    <select id="getRecordsCountByTypes" parameterType="map" resultType="Long">
        SELECT COUNT(*)
        FROM RECORD r
        <if test="organizationId != null">
            JOIN entity e
            ON r.authority = e.entity_id
            OR r.partner = e.entity_id
        </if>
        WHERE
        (r.record_type = ''
        <foreach collection="recordTypes" item="element" index="index">
            OR r.record_type = #{element}
        </foreach>
        )
        <if test="organizationId != null">
            AND e.entity_id = #{organizationId}
        </if>
        ;
    </select>

    <select id="getAllRecordsShortBetweenRowsByTypes" parameterType="map" resultMap="selectRecordsResultMap">
        SELECT *
        FROM (SELECT
        row_number()
        OVER (
        ORDER BY r.record_id ),
        r.record_id,
        r.subject,
        r.amount_czk,
        r.record_type,
        ret.date,
        ret.retrieval_id,
        e.entity_id,
        e.name
        FROM RECORD r
        JOIN RETRIEVAL ret
        ON r.retrieval_id = ret.retrieval_id
        <choose>
            <when test="organizationId != null">
                JOIN ENTITY e
                ON e.entity_id = r.authority
                OR e.entity_id = r.partner
            </when>
            <when test="organizationId == null">
                JOIN ENTITY e
                ON e.entity_id = r.authority
            </when>
        </choose>
        WHERE
        (r.record_type = ''
        <foreach collection="recordTypes" item="element" index="index">
            OR r.record_type = #{element}
        </foreach>
        )
        <if test="organizationId != null">
            AND e.entity_id = #{organizationId}
        </if>
        ) x
        WHERE ROW_NUMBER BETWEEN #{rowNumStart} AND #{rowNumEnd};
    </select>

    <select id="getAllRecordsFullBetweenRowsByTypes" parameterType="map" resultMap="selectRecordsResultMap">
        SELECT *
        FROM (SELECT
        row_number()
        OVER (
        ORDER BY r.record_id ),
        r.*,
        ret.date,
        ret.retrieval_id,
        e.entity_id,
        e.name
        FROM RECORD r
        JOIN RETRIEVAL ret
        ON r.retrieval_id = ret.retrieval_id
        JOIN ENTITY e
        ON e.entity_id = r.authority
        WHERE r.record_type = ''
        <foreach collection="recordTypes" item="element" index="index">
            OR r.record_type = #{element}
        </foreach>
        ) x
        WHERE ROW_NUMBER BETWEEN #{rowNumStart} AND #{rowNumEnd};
    </select>

    <select id="getRecordById" parameterType="Long" resultMap="selectRecordsWithOrganizationResultMap">
        SELECT
            r.*,
            e.entity_id    AS authority_id,
            e.name         AS authority_name,
            e.ico          AS authority_ico,
            e.entity_type  AS authority_type,
            e2.entity_id   AS partner_id,
            e2.name        AS partner_name,
            e2.ico         AS partner_ico,
            e2.entity_type AS partner_type,
            ret.date,
            ret.retrieval_id
        FROM record r
            LEFT JOIN entity e
                ON r.authority = e.entity_id
            LEFT JOIN entity e2
                ON e2.entity_id = r.partner
            LEFT JOIN retrieval ret
                ON r.retrieval_id = ret.retrieval_id
        WHERE r.record_id = #{id}
    </select>

    <resultMap id="selectRecordsResultMap" type="cz.cvut.fit.opendata.model.Record">
        <result property="recordId" column="record_id"/>
        <result property="amountCZK" column="amount_czk"/>
        <result property="authorityIdentifier" column="authority_identifier"/>
        <result property="currency" column="currency"/>
        <result property="dateCreated" column="date_created"/>
        <result property="dateOfExpiry" column="date_of_expiry"/>
        <result property="dateOfPayment" column="date_of_payment"/>
        <result property="dueDate" column="due_date"/>
        <result property="inEffect" column="in_effect"/>
        <result property="masterId" column="master_id"/>
        <result property="originalCurrencyAmount" column="original_currency_amount"/>
        <result property="subject" column="subject"/>
        <result property="variableSymbol" column="variable_symbol"/>
        <result property="recordType" column="record_type"/>
        <result property="authorityRole" column="authority_role"/>
        <result property="budgetCategory" column="budget_category"/>
        <result property="periodicity" column="periodicity"/>
        <association property="authority" column="authority" resultMap="selectEntitiesResultMap"
                     javaType="cz.cvut.fit.opendata.model.Entity"/>
        <association property="partner" column="partner" resultMap="selectEntitiesResultMap"
                     javaType="cz.cvut.fit.opendata.model.Entity"/>
        <!--<association property="parentRecord" column="parent_id" resultMap="recordResultMap" javaType="cz.cvut.fit.opendata.model.Record"/>-->
        <association property="retrieval" column="retrieval_id" resultMap="selectRetrievalsResultMap"
                     javaType="cz.cvut.fit.opendata.model.Retrieval"/>
    </resultMap>

    <resultMap id="selectRetrievalsResultMap" type="cz.cvut.fit.opendata.model.Retrieval">
        <result property="retrievalId" column="retrieval_id"/>
        <result property="date" column="date"/>
        <result property="failureReason" column="failure_reason"/>
        <result property="numberBadRecords" column="num_bad_records"/>
        <result property="numberRecordsInserted" column="num_records_inserted"/>
        <result property="success" column="success"/>
        <!--<association property="dataInstance" column="data_instance_id" resultMap="selectDataInstancesResultMap" javaType="cz.cvut.fit.opendata.model.DataInstance"/>-->
    </resultMap>

    <resultMap id="selectEntitiesResultMap" type="cz.cvut.fit.opendata.model.Entity">
        <result property="id" column="entity_id"/>
        <result property="dic" column="dic"/>
        <result property="ico" column="ico"/>
        <result property="name" column="name"/>
        <result property="isPublic" column="is_public"/>
        <result property="entityType" column="entity_type"/>
    </resultMap>

    <resultMap id="selectRecordsWithOrganizationResultMap" type="cz.cvut.fit.opendata.model.Record">
        <result property="recordId" column="record_id"/>
        <result property="amountCZK" column="amount_czk"/>
        <result property="authorityIdentifier" column="authority_identifier"/>
        <result property="currency" column="currency"/>
        <result property="dateCreated" column="date_created"/>
        <result property="dateOfExpiry" column="date_of_expiry"/>
        <result property="dateOfPayment" column="date_of_payment"/>
        <result property="dueDate" column="due_date"/>
        <result property="inEffect" column="in_effect"/>
        <result property="masterId" column="master_id"/>
        <result property="originalCurrencyAmount" column="original_currency_amount"/>
        <result property="subject" column="subject"/>
        <result property="variableSymbol" column="variable_symbol"/>
        <result property="recordType" column="record_type"/>
        <result property="authorityRole" column="authority_role"/>
        <result property="budgetCategory" column="budget_category"/>
        <result property="periodicity" column="periodicity"/>
        <association property="authority" column="authority"
                     javaType="cz.cvut.fit.opendata.model.Entity">
            <result property="id" column="authority_id"/>
            <result property="name" column="authority_name"/>
            <result property="ico" column="authority_ico"/>
            <result property="entityType" column="authority_type"/>
        </association>
        <association property="partner" column="partner"
                     javaType="cz.cvut.fit.opendata.model.Entity">
            <result property="id" column="partner_id"/>
            <result property="name" column="partner_name"/>
            <result property="ico" column="partner_ico"/>
            <result property="entityType" column="partner_type"/>
        </association>
        <association property="retrieval" column="retrieval_id" resultMap="selectRetrievalsResultMap"
                     javaType="cz.cvut.fit.opendata.model.Retrieval"/>
    </resultMap>

</mapper>