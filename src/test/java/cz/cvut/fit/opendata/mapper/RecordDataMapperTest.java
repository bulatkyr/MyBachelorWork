package cz.cvut.fit.opendata.mapper;

import cz.cvut.fit.opendata.OpendataApplication;
import cz.cvut.fit.opendata.model.Entity;
import cz.cvut.fit.opendata.model.Record;
import cz.cvut.fit.opendata.model.Retrieval;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static cz.cvut.fit.opendata.model.RecordType.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OpendataApplication.class)
@ActiveProfiles("test")
public class RecordDataMapperTest {

    private final Date FIFTH_OCTOBER_2017 = new GregorianCalendar(2017, Calendar.OCTOBER, 5).getTime();

    @Autowired
    private RecordDataMapper mapper;

    /**
     * Tests getting number of records filtered by single type of record
     */
    @Test
    public void getRecordsCountByTypesSingleTypeTest() {
        List<String> recordTypes = new ArrayList<>();
        recordTypes.add(invoice.getType());
        Long result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(5L, result.longValue());

        recordTypes.clear();
        recordTypes.add(payment.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(5L, result.longValue());

        recordTypes.clear();
        recordTypes.add(contract.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(5L, result.longValue());

        recordTypes.clear();
        recordTypes.add(order.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(5L, result.longValue());
    }

    /**
     * Tests getting number of record by several types of record
     */
    @Test
    public void getRecordsCountByTypesSeveralTypesTest() {
        List<String> recordTypes = new ArrayList<>();
        recordTypes.add(invoice.getType());
        recordTypes.add(payment.getType());
        Long result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(10L, result.longValue());

        recordTypes.clear();
        recordTypes.add(contract.getType());
        recordTypes.add(order.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(10L, result.longValue());

        recordTypes.clear();
        recordTypes.add(invoice.getType());
        recordTypes.add(contract.getType());
        recordTypes.add(order.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(15L, result.longValue());

        recordTypes.clear();
        recordTypes.add(order.getType());
        recordTypes.add(payment.getType());
        recordTypes.add(contract.getType());
        recordTypes.add(invoice.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, null);
        assertEquals(20L, result.longValue());
    }

    /**
     * Tests getting number of records by single type of record and authorityId
     */
    @Test
    public void getRecordsCountByTypesSingleTypeOrganizationTest() {
        List<String> recordTypes = new ArrayList<>();
        Long organizationId = 2L;
        recordTypes.add(invoice.getType());
        Long result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(1L, result.longValue());

        recordTypes.clear();
        organizationId = 3L;
        recordTypes.add(payment.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(2L, result.longValue());

        recordTypes.clear();
        organizationId = 4L;
        recordTypes.add(contract.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(1L, result.longValue());

        recordTypes.clear();
        organizationId = 5L;
        recordTypes.add(order.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(3L, result.longValue());
    }

    /**
     * Tests getting number of records by several types of record and authorityId
     */
    @Test
    public void getRecordsCountByTypesSeveralTypeOrganizationTest() {
        List<String> recordTypes = new ArrayList<>();
        Long organizationId = 2L;
        recordTypes.add(invoice.getType());
        recordTypes.add(order.getType());
        Long result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(3L, result.longValue());

        recordTypes.clear();
        organizationId = 3L;
        recordTypes.add(invoice.getType());
        recordTypes.add(payment.getType());
        recordTypes.add(contract.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(4L, result.longValue());

        recordTypes.clear();
        organizationId = 4L;
        recordTypes.add(contract.getType());
        recordTypes.add(payment.getType());
        recordTypes.add(invoice.getType());
        recordTypes.add(order.getType());
        result = mapper.getRecordsCountByTypes(recordTypes, organizationId);
        assertEquals(3L, result.longValue());
    }

    /**
     * Tests getting short description for record by record type and authorityId
     */
    @Test
    public void getAllRecordsShortBetweenRowsByTypesTest() {
        List<String> recordTypes = new ArrayList<>();
        Long organizationId = 6L;
        recordTypes.add(invoice.getType());
        List<Record> result = mapper.getAllRecordsShortBetweenRowsByTypes(1L, 10L, recordTypes, organizationId);
        assertNotNull(result);
        assertEquals(1, result.size());
        Record record = result.get(0);
        assertEquals(5L, record.getRecordId().longValue());
        assertEquals("subject5", record.getSubject());
        assertEquals(invoice, record.getRecordType());
        assertEquals(5.0, record.getAmountCZK(), 0D);
        Retrieval retrieval = record.getRetrieval();
        assertNotNull(retrieval);
        assertEquals(FIFTH_OCTOBER_2017, retrieval.getDate());
    }

    /**
     * Tests number of records and full record's info by record type
     */
    @Test
    public void getAllRecordsFullBetweenRowsByTypes() {
        List<String> recordTypes = new ArrayList<>();
        recordTypes.add(payment.getType());
        List<Record> result = mapper.getAllRecordsFullBetweenRowsByTypes(1L, 10L, recordTypes);
        assertNotNull(result);
        assertEquals(5, result.size());
        for (Record record : result) {
            assertEquals(6.6, record.getOriginalCurrencyAmount(), 0D);
            Retrieval retrieval = record.getRetrieval();
            assertNotNull(retrieval);
            assertEquals(FIFTH_OCTOBER_2017, retrieval.getDate());
            Entity authority = record.getAuthority();
            assertNotNull(authority);
            assertEquals("Profinit", authority.getName());
        }
    }
}