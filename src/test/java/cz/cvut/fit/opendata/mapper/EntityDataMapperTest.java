package cz.cvut.fit.opendata.mapper;

import cz.cvut.fit.opendata.model.Entity;
import cz.cvut.fit.opendata.model.Record;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fit.opendata.model.EntityType.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EntityDataMapperTest {

    @Autowired
    private EntityDataMapper entityDataMapper;

    /**
     * Tests correct counting of entities depending on one entity type
     * Each test checks count of rows for each type
     */
    @Test
    public void getEntitiesCountByTypes() {
        List<String> entityTypes = new ArrayList<>();
        entityTypes.add(individual.getType());
        Long result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(3L, result.longValue());

        entityTypes.clear();
        entityTypes.add(company.getType());
        result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(2L, result.longValue());

        entityTypes.clear();
        entityTypes.add(ministry.getType());
        result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(2L, result.longValue());

        entityTypes.clear();
        entityTypes.add(other.getType());
        result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(2L, result.longValue());
    }

    /**
     * Tests correct counting of entities depending on several entity types
     * Each test checks different combination of entity types
     */
    @Test
    public void getEntitiesCountBySeveralTypes() {
        List<String> entityTypes = new ArrayList<>();
        entityTypes.add(individual.getType());
        entityTypes.add(other.getType());
        Long result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(5L, result.longValue());

        entityTypes.clear();
        entityTypes.add(company.getType());
        entityTypes.add(ministry.getType());
        result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(4L, result.longValue());

        entityTypes.clear();
        entityTypes.add(ministry.getType());
        entityTypes.add(individual.getType());
        entityTypes.add(other.getType());
        result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(7L, result.longValue());

        entityTypes.clear();
        entityTypes.add(other.getType());
        entityTypes.add(individual.getType());
        entityTypes.add(ministry.getType());
        entityTypes.add(company.getType());
        result = entityDataMapper.getEntitiesCountByTypes(entityTypes);
        assertEquals(9L, result.longValue());
    }

    /**
     * Tests correct values of entities selected by entity type
     */
    @Test
    public void getAllEntitiesShortBetweenRowsByTypes() {
        List<String> entityTypes = new ArrayList<>();
        entityTypes.add(ministry.getType());
        List<Entity> result = entityDataMapper.getAllEntitiesShortBetweenRowsByTypes(1L, 10L, entityTypes);
        assertNotNull(result);
        assertEquals(2, result.size());
        for (Entity entity : result) {
            assertEquals(ministry, entity.getEntityType());
            assertEquals("Profinit", entity.getName());
            if ("333333333".equals(entity.getIco())) {
                assertEquals("333333333", entity.getIco());
            } else if ("444444444".equals(entity.getIco())) {
                assertEquals("444444444", entity.getIco());
            } else {
                fail();
            }
        }
    }

    /**
     * Tests correct values if entity selected by id.
     * Tests correct count of documents as authority and as partner for this entity selected by id
     */
    @Test
    public void getEntityDetailById() {
        Long entityId = 2L;
        Entity result = entityDataMapper.getEntityDetailById(entityId);
        assertNotNull(result);
        assertEquals("Profinit", result.getName());
        assertEquals("222222222", result.getIco());
        assertEquals("1112223344", result.getDic());
        assertEquals(company, result.getEntityType());
        List<Record> recordsAsAuthority = result.getRecordsAsAuthority();
        assertNotNull(recordsAsAuthority);
        assertEquals(3, recordsAsAuthority.size());
        result.setRecordsAsPartner(entityDataMapper.getRecordsAsPartnerByEntityId(entityId));
        List<Record> recordsAsPartner = result.getRecordsAsPartner();
        assertNotNull(recordsAsPartner);
        assertEquals(3, recordsAsPartner.size());
    }

    /**
     * Tests helper method for selecting documents for entity by id
     */
    @Test
    public void getRecordsAsPartnerByEntityId() {
        Long entityId = 5L;
        List<Record> recordsAsPartner = entityDataMapper.getRecordsAsPartnerByEntityId(entityId);
        assertNotNull(recordsAsPartner);
        assertEquals(4L, recordsAsPartner.size());
    }

    /**
     * Tests correct values of entity selected by ICO
     */
    @Test
    public void getEntityByIco() {
        String ico = "99999999";
        Entity result = entityDataMapper.getEntityByIco(ico);
        assertNotNull(result);
        assertEquals(ico, result.getIco());
        assertEquals("1112223344", result.getDic());
        assertEquals("Profinit", result.getName());
        assertEquals(9L, result.getId().longValue());
        assertEquals(other, result.getEntityType());
    }
}