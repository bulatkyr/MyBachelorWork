package cz.cvut.fit.opendata.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


@SuppressWarnings("SameParameterValue")
@RunWith(SpringRunner.class)
@SpringBootTest
public class LinkResolverServiceTest {


    private static final String RECORDS_PATH = "/documents";
    private static final String ORGANIZATIONS_PATH = "/documents";
    @Autowired
    private LinkResolverService service;

    private MockHttpServletRequest request;

    @Before
    public void setUp() {
        this.request = new MockHttpServletRequest();
        this.request.setScheme("http");
        this.request.setServerName("www.opendatacr.cz");
        this.request.setServerPort(-1);
    }

    private URI preparePageLink(String replacePath, Long pageNumber) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme("http").host("www.opendatacr.cz")
                .path(replacePath);
        if (pageNumber != null) {
            return uriComponentsBuilder.query("page={keyword}").buildAndExpand(pageNumber).toUri();
        }
        return uriComponentsBuilder.build().toUri();
    }

    private void testEmptyQueryString(String testedPath, long expectedPageNumber, long failPageNumber) {
        this.request.setRequestURI(testedPath);
        this.request.setContextPath(testedPath);

        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(this.request);
        URI testedUri = service.getPageUri(builder, expectedPageNumber);
        URI expectedUri = preparePageLink(testedPath, expectedPageNumber);

        assertNotEquals(preparePageLink(testedPath, failPageNumber), testedUri);
        assertEquals(expectedUri, testedUri);
    }

    /**
     * Tests replacement of page number with empty query string for records page
     */
    @Test
    public void recordsEmptyPathLinkTest() {
        testEmptyQueryString(RECORDS_PATH, 5L, 6L);
    }

    /**
     * Tests replacement of page number with empty query string for organizations page
     */
    @Test
    public void organizationsEmptyPathLinkTest() {
        testEmptyQueryString(ORGANIZATIONS_PATH, 6L, 8L);
    }

    private void testFilledQueryString(String testedPath, long actualPageNumber, long expectedPageNumber, long failPageNumber) {
        this.request.setRequestURI(testedPath);
        this.request.setContextPath(testedPath);
        this.request.setQueryString("page=" + actualPageNumber);

        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(this.request);
        URI testedUri = service.getPageUri(builder, expectedPageNumber);
        URI expectedUri = preparePageLink(testedPath, expectedPageNumber);

        assertNotEquals(preparePageLink(testedPath, failPageNumber), testedUri);
        assertEquals(expectedUri, testedUri);
    }

    /**
     * Tests replacement of page number with filled query string with actual page number for records page
     */
    @Test
    public void recordsFilledPathLinkTest() {
        testFilledQueryString(RECORDS_PATH, 0, 5L, 6L);
    }

    /**
     * Tests replacement of page number with filled query string with actual page number for organizations page
     */
    @Test
    public void organizationsFilledPathLinkTest() {
        testFilledQueryString(ORGANIZATIONS_PATH, 1, 6L, 8L);
    }

    private void testReplacementOfPathEmptyQueryString(String replacedPath, String actualPath) {
        this.request.setRequestURI(actualPath);
        this.request.setContextPath(actualPath);

        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(this.request);
        URI testedUri = service.getPageUriReplacedPath(builder, replacedPath);
        URI expectedUri = preparePageLink(replacedPath, null);

        assertEquals(expectedUri, testedUri);
    }

    private void testReplacementOfPathFilledQueryString(String replacedPath, String actualPath) {
        this.request.setRequestURI(actualPath);
        this.request.setContextPath(actualPath);
        //random queryString
        this.request.setQueryString("page=10&organizationId=11");

        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(this.request);
        URI testedUri = service.getPageUriReplacedPath(builder, replacedPath);
        URI expectedUri = preparePageLink(replacedPath, null);

        assertEquals(expectedUri, testedUri);
    }


    /**
     * Test replacement of the path with empty query string.
     */
    @Test
    public void emptyQueryStringHomePageReplaceTest() {
        testReplacementOfPathEmptyQueryString("home", RECORDS_PATH);
    }

    /**
     * Test replacement of the path with filled query string.
     */
    @Test
    public void filledQueryStringApiPageReplaceTest() {
        testReplacementOfPathFilledQueryString("api", ORGANIZATIONS_PATH);
    }


}