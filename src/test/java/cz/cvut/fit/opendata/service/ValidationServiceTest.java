package cz.cvut.fit.opendata.service;

import cz.cvut.fit.opendata.model.exception.ValidationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fit.opendata.model.EntityType.*;
import static cz.cvut.fit.opendata.model.RecordType.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidationServiceTest {

    private final static String COMMA_SEPARATOR = ",";

    @Autowired
    private ValidationService service;

    /**
     * Tests logic of service method to not accept negative numbers
     *
     * @throws ValidationException - exception to check
     */
    @Test(expected = ValidationException.class)
    public void validationLong() throws ValidationException {
        service.validateNonNegativeLong(-5L);
    }

    /**
     * Tests correct throwing of validation exception during validation record type passed to service method
     *
     * @throws ValidationException - exception to check
     */
    @Test(expected = ValidationException.class)
    public void validateRecordTypeIncorrect() throws ValidationException {
        String incorrectRecordType = "invoise";
        service.validateRecordType(incorrectRecordType);
    }

    /**
     * Tests correct validation of different record types. Useful for future development of application.
     *
     * @throws ValidationException - exception to check
     */
    @Test
    public void validateRecordTypeCorrect() throws ValidationException {
        service.validateRecordType(invoice.getType());
        service.validateRecordType(payment.getType());
        service.validateRecordType(order.getType());
        service.validateRecordType(contract.getType());
    }

    /**
     * Tests correct parsing of record types.
     * Tests correct throwing of exception in case of incorrect record type in input string
     *
     * @throws ValidationException - exception to check
     */
    @Test
    public void parseRecordTypes() throws ValidationException {
        String recordTypes = invoice.getType();
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add(invoice.getType());
        List<String> result = service.parseRecordTypes(recordTypes);
        assertEquals(expectedResult, result);

        recordTypes += COMMA_SEPARATOR + contract;
        expectedResult.add(contract.getType());
        result = service.parseRecordTypes(recordTypes);
        assertEquals(expectedResult, result);

        recordTypes += COMMA_SEPARATOR + payment;
        expectedResult.add(payment.getType());
        result = service.parseRecordTypes(recordTypes);
        assertEquals(expectedResult, result);

        recordTypes += COMMA_SEPARATOR + order;
        expectedResult.add(order.getType());
        result = service.parseRecordTypes(recordTypes);
        assertEquals(expectedResult, result);

        // There is no problem for application to have the same type of record several times, can be fixed in future version
        recordTypes += COMMA_SEPARATOR + order;
        expectedResult.add(order.getType());
        result = service.parseRecordTypes(recordTypes);
        assertEquals(expectedResult, result);

        recordTypes += COMMA_SEPARATOR + "platba";
        try {
            service.parseRecordTypes(recordTypes);
            fail();
        } catch (ValidationException ve) {
            assertEquals("Record Type platba is not recognized", ve.getMessage());
        }
    }

    /**
     * Tests correct parsing of entity types
     * Test throwing exception in case of incorrect entity types
     *
     * @throws ValidationException - exception to check
     */
    @Test
    public void parseEntityTypes() throws ValidationException {
        String entityTypes = company.getType();
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add(company.getType());
        List<String> result = service.parseEntityTypes(entityTypes);
        assertEquals(expectedResult, result);

        entityTypes += COMMA_SEPARATOR + individual;
        expectedResult.add(individual.getType());
        result = service.parseEntityTypes(entityTypes);
        assertEquals(expectedResult, result);

        entityTypes += COMMA_SEPARATOR + ministry;
        expectedResult.add(ministry.getType());
        result = service.parseEntityTypes(entityTypes);
        assertEquals(expectedResult, result);

        entityTypes += COMMA_SEPARATOR + other;
        expectedResult.add(other.getType());
        result = service.parseEntityTypes(entityTypes);
        assertEquals(expectedResult, result);

        // There is no problem for application to have the same type of entity several times, can be fixed in future version
        entityTypes += COMMA_SEPARATOR + individual;
        expectedResult.add(individual.getType());
        result = service.parseEntityTypes(entityTypes);
        assertEquals(expectedResult, result);

        entityTypes += COMMA_SEPARATOR + "ministerstvo";
        try {
            service.parseEntityTypes(entityTypes);
            fail();
        } catch (ValidationException ve) {
            assertEquals("Organization Type ministerstvo is not recognized", ve.getMessage());
        }
    }

    /**
     * Test validation of entity type and throwing correct exception
     *
     * @throws ValidationException - exception to check
     */
    @Test(expected = ValidationException.class)
    public void validateEntityTypeIncorrect() throws ValidationException {
        String incorrectEntityType = "firma";
        service.validateEntityType(incorrectEntityType);
    }

    /**
     * Tests checks correct validation of ico in case of correct ICO
     *
     * @throws ValidationException - possible exception
     */
    @Test
    public void validateIcoTest() throws ValidationException {
        String ico = "25596641";
        String result = service.validateIco(ico);
        assertEquals(result, ico);
    }

    /**
     * Tests validates null value passed to ICO
     *
     * @throws ValidationException - thrown exception in case of null value ICO
     */
    @Test(expected = ValidationException.class)
    public void validateIcoTest1() throws ValidationException {
        try {
            String result = service.validateIco(null);
        } catch (ValidationException ex) {
            assertEquals("ICO is not specified", ex.getMessage());
            throw ex;
        }
        fail();
    }

    /**
     * Tests validates correct behaviour of method in case of ICO with characters
     *
     * @throws ValidationException - thrown exception in case if ICO has characters
     */
    @Test(expected = ValidationException.class)
    public void validateIcoTest2() throws ValidationException {
        String ico = "aa123456";
        try {
            String result = service.validateIco(ico);
        } catch (ValidationException ex) {
            assertEquals("ICO " + ico + " has to contain only numbers", ex.getMessage());
            throw ex;
        }
        fail();
    }

    /**
     * Tests validates correct behaviour in case of ico is not sent with zeroes at the start
     *
     * @throws ValidationException - possible validation exception
     */
    @Test
    public void validateIcoTest3() throws ValidationException {
        String ico = "6947";
        String result = service.validateIco(ico);
        assertEquals("0000" + ico, result);
    }

    /**
     * Test validates business logic of method for validation ICO, ICO passed to method is incorrect
     *
     * @throws ValidationException - thrown exception in case of incorrect value of ICO (not allowed ICO)
     */
    @Test(expected = ValidationException.class)
    public void validateIcoTest4() throws ValidationException {
        String ico = "6948";
        try {
            String result = service.validateIco(ico);
        } catch (ValidationException ex) {
            assertEquals("ICO is not specified correctly", ex.getMessage());
            throw ex;
        }
        fail();
    }
}