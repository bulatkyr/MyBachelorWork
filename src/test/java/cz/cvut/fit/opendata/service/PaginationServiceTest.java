package cz.cvut.fit.opendata.service;

import cz.cvut.fit.opendata.model.exception.ValidationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaginationServiceTest {

    @Autowired
    PaginationService paginationService;

    @Test(expected = ValidationException.class)
    public void validateCurrentPage() throws ValidationException {
        paginationService.validateCurrentPage(null, 100L);
    }

    @Test(expected = ValidationException.class)
    public void validateCurrentPage1() throws ValidationException {
        paginationService.validateCurrentPage(-1L, 100L);
    }

    @Test(expected = ValidationException.class)
    public void validateCurrentPage2() throws ValidationException {
        paginationService.validateCurrentPage(1L, null);
    }

    @Test(expected = ValidationException.class)
    public void validateCurrentPage3() throws ValidationException {
        paginationService.validateCurrentPage(1L, -1L);
    }

    /**
     * Covers logic of validateCurrentPage
     * Tests logic of method in case of incorrect values of current page and rows count
     *
     * @throws ValidationException - exception to catch
     */
    @Test(expected = ValidationException.class)
    public void validateCurrentPage4() throws ValidationException {
        Long currentPage = 3L;
        Long cntRows = 20L;
        try {
            paginationService.validateCurrentPage(currentPage, cntRows);
        } catch (ValidationException ex) {
            String exceptionMessage = ex.getMessage();
            assertEquals(exceptionMessage, "Current page " + currentPage + " cannot be higher than last page value.");
            throw ex;
        }
    }

    /**
     * Covers logic of countNextPage
     * 1. test is on counting next page in case of current page is not the last page
     * 2. test is on counting next page in case of current page is already lat page
     */
    @Test
    public void countNextPage() {
        Long currentPage = 2L;
        Long rowsCnt = 35L;
        Long nextPage = paginationService.countNextPage(currentPage, rowsCnt);
        assertEquals(nextPage.longValue(), 3L);
        currentPage = 3L;
        nextPage = paginationService.countNextPage(currentPage, rowsCnt);
        assertEquals(nextPage.longValue(), 3L);
    }

    /**
     * Covers logic of countPreviousPage
     * 1. test is on counting previous page in case of usual current page number
     * 2. test is on counting previous page in case of first current page number
     * 3. test is on counting previous page in case of incorrect current page number
     */
    @Test
    public void countPreviousPage() {
        Long currentPage = 2L;
        Long rowsCnt = 35L;
        Long previousPage = paginationService.countPreviousPage(currentPage, rowsCnt);
        assertEquals(previousPage.longValue(), 1L);
        currentPage = 0L;
        previousPage = paginationService.countPreviousPage(currentPage, rowsCnt);
        assertEquals(previousPage.longValue(), 0);
        currentPage = -1L;
        previousPage = paginationService.countPreviousPage(currentPage, rowsCnt);
        assertEquals(previousPage.longValue(), currentPage.longValue());
    }

    /**
     * Covers logic of countStartRow
     * 1. test is on counting start row in case of usual current page number
     * 2. test is on counting start row in case of first current page number
     */
    @Test
    public void countStartRow() {
        Long currentPage = 2L;
        Long rowsCnt = 35L;
        Long startRow = paginationService.countStartRow(currentPage, rowsCnt);
        assertEquals(21L, startRow.longValue());
        currentPage = 0L;
        startRow = paginationService.countStartRow(currentPage, rowsCnt);
        assertEquals(1L, startRow.longValue());
    }

    /**
     * Covers logic of countEndRow
     * 1. test is on counting end row in usual case of number of current page
     * 2. test is on counting end row in special case of first page or less then first page
     */
    @Test
    public void countEndRow() {
        Long currentPage = 2L;
        Long rowsCnt = 35L;
        Long endRow = paginationService.countEndRow(currentPage, rowsCnt);
        assertEquals(30L, endRow.longValue());
        currentPage = 0L;
        endRow = paginationService.countEndRow(currentPage, rowsCnt);
        assertEquals(10L, endRow.longValue());
    }

    /**
     * Covers logic of countFirstPage
     */
    @Test
    public void countFirstPage() {
        assertEquals(0L, paginationService.countFirstPage().longValue());
    }

    /**
     * Covers logic of countLastPage
     * 1. test is on counting last page with not fully divided count of rows
     * 2. test is on counting last page with fully divided count of rows
     */
    @Test
    public void countLastPage() {
        Long rowsCnt = 25L;
        Long lastPage = paginationService.countLastPage(rowsCnt);
        assertEquals(lastPage.longValue(), 2L);
        rowsCnt = 30L;
        lastPage = paginationService.countLastPage(rowsCnt);
        assertEquals(lastPage.longValue(), 2L);
    }

    /**
     * Covers logic of countCurrentPage method
     */
    @Test
    public void countCurrentPage() {
        Long currentPage = 1L;
        assertEquals(currentPage, paginationService.countCurrentPage(currentPage));
    }
}